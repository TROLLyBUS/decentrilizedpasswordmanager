# Узел сети блокчейна

![Python Version](https://img.shields.io/badge/python-3.8+-blue?style=for-the-badge&logo=python)

Узел сети блокчейна децентрализованного хранилища паролей

## Описание
Элементарный узел сети блокчейна, который содержит в себе данные децентрализованного хранилища. Присоединение происходит путем загрузки исходного кода и некоторого, возможно неполного списка актуальных узлов сети. После развертывания происходит этап консенсуса, в который выполняется синхронизация с предзагруженными узлами, получение полного списка узлов и актуального состояния цепи блокчейна сети.

## Запуск узла
Необходимо клонировать этот репозиторий с помощью `git clone`, установить зависимости и запустить `node.py` файл с необходимыми параметрами.

*Команды для запуска узла*
```
user@host:~$ git clone https://github.com/6dba/decentralized-password-manager
user@host:~$ cd decentralized-password-manager/server
user@host:~/decentralized-password-manager/server$ pip install -r requirements.txt
user@host:~/decentralized-password-manager/server$ python node.py --host 192.168.0.77 -p 5100 -lp 5100 -f nodes.example.json
user@host:~/decentralized-password-manager/server$ curl -X GET "http://192.168.0.77:5100/consensus"
```
После запуска узел может быть доступен по `http://host:port`, если проводится запуск вне NAT, то `-p` `-lp` могут быть одинаковыми. Параметры `--host` и `--port` созданы для запуска узлов сети за NAT. Данные значения являются "белым" IP адресом, который может быть использован для доступа к текущему узлу другими узлами из вне. Для корректного запуска узлу должен быть предоставлен минимум один адрес существующего, актуального узла сети. Также, после первого запуска обязательно должен быть выполнен алгоритм консенсуса `/consensus`. По URL узла доступна [Swagger OpenAPI](https://swagger.io/specification/) документация. Развертывание узла сети подразумевает загрузку исходного кода, с псевдо централизованного ресурса, который будет содержать часть или полное количество уже существующих узлов сети в `nodes.json`. Целостность исходного кода, как вариант, можно проверять контрольной суммой.

## Использование
```
node.py [-h] --host HOST -p PORT [-lp LOCAL_PORT] -f FILE

options:
  -h, --help            show this help message and exit
  --host HOST           the public IPv4 address of the node available on the Internet. Required to reach a node by other nodes
  -p PORT, --port PORT  the public port of the node, accessible on the Internet. Required to reach a node by other nodes
  -lp LOCAL_PORT, --local-port LOCAL_PORT
                        the host port of the node where the blockchain API will be deployed
  -f FILE, --file FILE  the path to the file containing the list of node addresses
```
### Пример `nodes.json` файла
```json
{
    "nodes": [
        "192.168.0.77:5100"
    ]
}
```