from datetime import datetime
from typing import Any, Optional
from uuid import UUID
from pydantic import BaseModel

from ...common.errors import STORAGE_EMPTY, UUID_EXISTS, UUID_NOT_FOUND
from .Data.UpdateDataModel import UpdateData
from .Data.DataModel import Data


class Storage(BaseModel):
    """
    Хранилище паролей. Представляет собой структуру данных вида:\n
    dict[str, list[Data(str, str, str, str, datetime, datetime)] и соотвествующие CRUD операции.\n
    Где: key - публичный ключ пользователя; value - список ресурсов и паролей к ним в виде объекта Data.
    """
    storage: dict[str, list[Data]]

    def __init__(__pydantic_self__, **data: Any) -> None:
        super().__init__(storage={"root": [Data(id=UUID(
            int=0x0), resource="root", password="root")]})

    def __index(self, iterable: list[Data], id: Optional[UUID | None] = None, compared: Optional[Data | None] = None) -> int | None:
        """
        Получение индекса объекта Data в списке хранилища iterable 
        по идентификатору или compared данным.
 
        :param Optional[UUID | None] id: Идентификатор ресурса
        :param Optional[Data | None] compared: Данные к сравнению (поиску)
        :return: Индекс обьекта в списке или None
        :rtype: int or None
        """
        if not iterable:
            return None

        sought = next((item for item in iterable if id == item.id or compared ==
                      item), None)
        if not sought:
            return None

        return iterable.index(sought)

    def insert(self, key: str, data: Data) -> Data:
        """
        Вставка нового объекта данных data в список данных ключа key.
        Дублирование объектов с одинаковым UUID не допускается.

        :param str key: Публичный ключ пользователя
        :param Data data: Ресурс
        :return: Копия успешно добавленных данных
        :rtype: Data
        :raises ValueError: Добавление ресурса с уже имеющимся в хранилище UUID
        """
        if (self.__index(self.storage.get(key, None), data.id) is not None):
            raise ValueError(UUID_EXISTS)

        # https://stackoverflow.com/questions/2052111/efficient-way-to-either-create-a-list-or-append-to-it-if-one-already-exists
        self.storage.setdefault(key, []).append(data.copy())

        return self.storage.get(key)[self.__index(self.storage.get(key), compared=data)].copy()

    def update(self, key: str, id: UUID, new_data: UpdateData) -> Data:
        """
        Обновление объекта данных Data в списке данных ключа key по идентификатору ресурса.

        :param str key: Публичный ключ пользователя
        :param UUID id: Идентификатор ресурса в хранилище
        :param UpdateData new_data: Обновленные данные ресурса
        :return: Копия успешно обновленных данных
        :rtype: Data
        :raises AttributeError: Хранилище по данному key пустое или UUID ресурса не найден 
        """
        if not key in self.storage:
            raise AttributeError(STORAGE_EMPTY)

        index = self.__index(self.storage.get(key), id)

        if index is None:
            raise AttributeError(UUID_NOT_FOUND)

        # Заменяем старые данные, данными новой модели, обновляем время изменения
        old_data = self.storage.get(key)[index]
        self.storage.get(key)[index] = old_data.copy(
            update={**new_data.dict(), **dict(updated_at=datetime.utcnow().isoformat())})

        return self.storage.get(key)[index].copy()

    def get(self, key: str, id: Optional[UUID | None] = None) -> list[Data] | Data | None:
        """
        Получение копии списка данных или копии объекта Data (если предоставлен корректный параметр id) ключа key из хранилища.

        :param str key: Публичный ключ пользователя
        :param Optional[UUID | None] id: Идентификатор ресурса в хранилище
        :return: Копия списка данных или копия объекта Data или None, если данные не найдены
        :rtype: list[Data] or Data or None
        """
        if not key in self.storage:
            return None
        
        if id:
            index = self.__index(self.storage.get(key), id)
            if index is None: 
                return None

            return self.storage.get(key)[index].copy()

        return self.storage.get(key).copy()

    def delete(self, key: str, id: Optional[UUID | None] = None, data: Optional[Data | None] = None) -> list[Data] | Data:
        """
        Удаление списка данных (если id и data - None) или объекта Data (если предоставлен корректный параметр id, 
        или хранящийся обьект data) ключа key из хранилища.

        :param str key: Публичный ключ пользователя
        :param Optional[UUID | None] id: Идентификатор ресурса в хранилище
        :param Optional[Data | None] data: Обьект данных, который находится в хранилище (по которому можно удалить идентичный обьект из хранилища)
        :return: Удаленные данные
        :rtype: list[Data] or Data
        :raises AttributeError: Хранилище по данному key пустое или UUID ресурса не найден 
        """
        if not key in self.storage:
            raise AttributeError(STORAGE_EMPTY)

        index = self.__index(self.storage.get(key), id, data)
        
        if (id or data) and index is None:
            raise AttributeError(UUID_NOT_FOUND)

        if index is not None:
            return self.storage.get(key).pop(index)

        return self.storage.pop(key)
