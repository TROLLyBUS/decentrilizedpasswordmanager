import json
import requests
from uuid import UUID
from flask import Flask, Response
from flask_restx import Api, Resource, reqparse

from api.models.resources import post_resource, patch_resource, post_nodes
from api.common.messages import BLOCK_NOT_MINED, CHAIN_MAJOR, CHAIN_REPLACED, NODES_INVALID, RESOURCE_STATE_CHANGED, ROOT_CANNOT_MODIFIED
from api.common.api_docs import docs
from blockchain.blockchain.Blockchain import Blockchain
from blockchain.storage.models.Data.DataModel import Data
from blockchain.storage.models.Data.UpdateDataModel import UpdateData

app = Flask(__name__)
api = Api(
    app,
    version="0.4.2",
    title="Blockchain API",
    description="API узла сети блокчейна.\n Author: https://t.me/TROLLyBUS",
    contact_url="https://t.me/TROLLyBUS",
    default="Документация",
    default_label="")

parser = reqparse.RequestParser()

blockchain = Blockchain()

@api.route('/chain')
@api.doc(responses={200: docs[200], 500: docs[500]})
class Chain(Resource):
    """
    Взаимодействие с цепочкой блокчейна.
    """

    def get(self) -> Response:
        """
        Получении цепи блокчейна узла.
        """
        # Если осуществлять сериализацию в JSON обьекта blockhain.chain,
        # то возникнет ошибка, так как Flask не знает способов разрешения сериализации обьектов Block и Storage.
        # Поэтому собственными методами преобразуем список обьектов в строку формата JSON,
        # затем десериализуем в python обьект. То есть json.loads() создает нативный обьект python из строки формата JSON,
        # который уже отправляется в response
        return {
            'length': len(blockchain.chain),
            'chain': json.loads(blockchain.json())
        }, 200


@api.route('/block')
@api.doc(responses={200: docs[200], 500: docs[500]})
class Block(Resource):
    """
    Взаимодействие с цепочкой блокчейна. Получение крайнего блока.
    """

    def get(self) -> Response:
        """
        Получение крайнего блока цепи блокчейна.
        """
        return {
            'block': json.loads(json.dumps(blockchain.last_block, default=blockchain.serializable))
        }, 200


@api.route('/nodes')
@api.doc(responses={500: docs[500]})
class Nodes(Resource):
    """
    Взаимодействие с узлами сети блокчейна.
    """
    @api.expect(post_nodes)
    @api.doc(responses={201: docs[201], 400: docs[400]})
    def post(self) -> Response:
        """
        Регистрация новых узлов в сети.
        """
        request_data = post_nodes.parse_args()
        nodes = request_data.get('nodes')

        if not isinstance(nodes, list):
            return {
                'message': NODES_INVALID
            }, 400

        for node in nodes:
            try:
                response = requests.get(f'http://{node}/nodes', timeout=3)
            except Exception:
                continue
            else:
                if response.status_code != 200:
                    continue

                try:
                    blockchain.register_node(node)
                except ValueError:
                    continue
                nodes = response.json()['nodes']
                blockchain.nodes.update(nodes)

        return {
            'count': len(blockchain.nodes),
            'nodes': list(blockchain.nodes),
        }, 201

    @api.doc(responses={200: docs[200]})
    def get(self) -> Response:
        """
        Получение списка узлов сети.
        """
        return {
            'count': len(blockchain.nodes),
            'nodes': list(blockchain.nodes),
        }, 200


@api.route('/consensus')
@api.doc(responses={500: docs[500]})
class Consensus(Resource):
    """
    Достижение консенсуса узлов сети блокчейна.
    """
    @api.doc(responses={200: docs[200]})
    def get(self) -> Response:
        """
        Разрешение конфликтов состояния цепи блокчейна между узлами сети.
        Опрос узлов, замена локальной копии цепи блокчейна, наиболее актуальной в сети.
        Синхронизация множества адресов узлов.
        """
        is_consensus = blockchain.consensus()
        message = CHAIN_MAJOR

        if is_consensus:
            message = CHAIN_REPLACED

        return {
            'replaced': is_consensus,
            'message': message,
            'length': len(blockchain.chain),
            'chain': json.loads(blockchain.json())
        }, 200


@api.route('/storage/<string:key>')
@api.doc(params={'key': 'Публичный ключ пользователя'}, responses={500: docs[500]})
class StorageKey(Resource):
    """
    Взаимодействие с хранилищем паролей блокчейна.
    """
    @api.doc(responses={200: docs[200], 404: docs[404]})
    def get(self, key: str) -> Response:
        """
        Получение хранилища публичного ключа.
        """
        blockchain.consensus()

        data = blockchain.get_data(key=key)

        if isinstance(data, list):
            return {
                'key': key,
                'storage': json.loads(json.dumps(obj=data, default=blockchain.serializable))
            }, 200

        return {
            'key': key,
            'storage': list()
        }, 404

    @api.expect(post_resource)
    @api.doc(responses={201: docs[201], 400: docs[400]})
    def post(self, key: str) -> Response:
        """
        Добавление данных в хранилище.
        В случае неудачной транзакции, предоставленные данные не будут добавлены.
        """
        if key == 'root':
            return {
                'key': key,
                'message': ROOT_CANNOT_MODIFIED
            }, 400

        request_data = post_resource.parse_args()
        resource = request_data.get('resource')
        password = request_data.get('password')
        description = request_data.get('description')

        data = Data(resource=resource, password=password,
                    description=description)

        blockchain.consensus()

        return {
            'index': blockchain.new_data(
                key=key, data=data
            ),
            'key': key,
            'resource_id': str(data.id),
            'message': RESOURCE_STATE_CHANGED
        }, 201

    @api.doc(responses={201: docs[201], 400: docs[400]})
    def delete(self, key: str) -> Response:
        """
        Удаление хранилища публичного ключа.
        В случае неудачной транзакции, предоставленные изменения не будут применены.
        """
        if key == 'root':
            return {
                'key': key,
                'message': ROOT_CANNOT_MODIFIED
            }, 400

        blockchain.consensus()

        return {
            'index': blockchain.delete_data(key=key),
            'key': key,
            'message': RESOURCE_STATE_CHANGED
        }, 201


@api.route('/storage/<string:key>/<string:id>')
@api.doc(params={
    'key': 'Публичный ключ пользователя',
    'id': 'Идентификатор ресурса'
}, responses={500: docs[500]})
class StorageKeyId(Resource):
    """
    Взаимодействие с ресурсом в хранилище паролей блокчейна.
    """
    @api.doc(responses={200: docs[200], 400: docs[400], 404: docs[404]})
    def get(self, key: str, id: str) -> Response:
        """
        Получение ресурса публичного ключа из хранилища.
        """
        blockchain.consensus()

        try:
            data = blockchain.get_data(key=key, obtained=UUID(id))
        except Exception as err:
            return {
                'key': key,
                'resource_id': id,
                'message': str(err)
            }, 400

        if isinstance(data, Data):
            return {
                'key': key,
                'resource_id': id,
                'resource': json.loads(data.json())
            }, 200

        return {
            'key': key,
            'resource_id': id,
            'resource': None
        }, 404

    @api.expect(patch_resource)
    @api.doc(responses={201: docs[201], 400: docs[400]})
    def patch(self, key: str, id: str) -> Response:
        """
        Изменение ресурса публичного ключа в хранилище.
        В случае неудачной транзакции, предоставленные изменения не будут применены.
        """
        if key == 'root':
            return {
                'key': key,
                'resource_id': id,
                'message': ROOT_CANNOT_MODIFIED
            }, 400

        request_data = patch_resource.parse_args()
        resource = request_data.get('resource')
        password = request_data.get('password')
        description = request_data.get('description')

        data = UpdateData(resource=resource, password=password,
                          description=description)

        blockchain.consensus()

        return {
            'index': blockchain.update_data(key=key, renewing=id, new_data=data),
            'key': key,
            'resource_id': id,
            'message': RESOURCE_STATE_CHANGED
        }, 201

    @api.doc(responses={201: docs[201], 400: docs[400]})
    def delete(self, key: str, id: str) -> Response:
        """
        Удаление ресурса публичного ключа в хранилище.
        В случае неудачной транзакции, предоставленные изменения не будут применены.
        """
        if key == 'root':
            return {
                'key': key,
                'resource_id': id,
                'message': ROOT_CANNOT_MODIFIED
            }, 400

        blockchain.consensus()

        return {
            'index': blockchain.delete_data(key=key, removal=id),
            'key': key,
            'resource_id': id,
            'message': RESOURCE_STATE_CHANGED
        }, 201


@api.route('/mine')
@api.doc(responses={500: docs[500]})
class Mine(Resource):
    """
    Добыча нового блока в сети блокчейна.
    """
    @api.doc(responses={201: docs[201], 400: docs[400]})
    def get(self) -> Response:
        """
        Добыча нового блока в сети блокчейна.
        """
        blockchain.consensus()
        proof, difficulty = blockchain.proof_of_work()
        block = blockchain.create_block(proof=proof, difficulty=difficulty)

        if block is not None:
            return {
                'block': json.loads(block.json())
            }, 201

        return {
            'message', BLOCK_NOT_MINED
        }, 400


@api.route('/alive')
@api.doc(responses={200: docs[200]})
class Alive(Resource):
    """
    Проверка состояния узлов сети блокчейна.
    """

    def get(self) -> Response:
        """
        Проверка состония узла.
        Запрашивающий узел принимает решение о доступности запрашиваемого узла.
        """
        return {
            'alive': True
        }, 200
