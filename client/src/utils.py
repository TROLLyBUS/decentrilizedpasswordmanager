def isEnglish(s: str) -> bool:
    """
    Может ли вводимый символ быть представлен в виде кодировки ASCII.
    Используется для проверки вводимых значений сид-фразы.

    :param s: Строковое значение (часть сид-фразы)
    :type s: str
    :return: Может ли вводимый символ быть представлен в виде кодировки ASCII. Если истино - строка потенциально может являться значением латинского алфавита.
    :rtype: bool
    """
    try:
        s.encode(encoding='utf-8').decode('ascii')
    except UnicodeDecodeError:
        return False
    else:
        return True
