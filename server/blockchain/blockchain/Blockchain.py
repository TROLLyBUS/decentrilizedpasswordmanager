import json
import requests

from ipaddress import IPv4Address
from typing import Any, Optional, Tuple
from uuid import UUID
from gostcrypto import gosthash

from blockchain.storage.models.Data.UpdateDataModel import UpdateData
from ..common.errors import INVALID_NODE_URL, INVALID_STORAGE_RECOVERY_TYPE
from ..storage.models.Data.DataModel import Data
from ..storage.models.StorageModel import Storage
from .models.BlockModel import Block


class Blockchain:
    """
    ## Имплементация блокчейна, хранящего в себе пароли пользователей.

    #### Опционально реализовать
    #TODO: Логгирование ошибок
    """

    def __init__(self):
        self.nodes: set[str] = set()

        # Данные, состояние которых будет изменено при выполнении следующей транзакции
        # (создании нового блока)
        self.__unconfirmed_insert: list[Tuple[str, Data]] = list()
        self.__unconfirmed_update: list[Tuple[str, UUID, UpdateData]] = list()
        self.__unconfirmed_delete: list[Tuple[str, UUID, Data]] = list()

        self.__chain: list[Block] = [self.__genesis()]

    def __genesis(self) -> Block:
        """
        Создание корневого блока.

        :return: Корневой блок
        :rtype: Block
        """
        return Block(
            index=1,
            proof=0,
            difficulty=1.0,
            previous_hash=str(),
            previous_proof=0,
            storage=Storage())

    def __storage_representation(self) -> Storage:
        """
        Восстановление модели хранилища Storage и вложенных моделей Data из обьекта хранилища в виде dict.
        Модель хранилища извлекается из крайнего блока, проверка типа хранилища (хранилище может пренадлежать типу Storage или dict) должна происходить в вызывающих методах.

        :return: Восстановленное хранилище
        :rtype: Storage
        :raises ValueError: Неверный тип хранилища для восстановления, крайний блок должен содержать dict
        """
        if isinstance(self.last_block['storage'], Storage):
            raise ValueError(INVALID_STORAGE_RECOVERY_TYPE)

        storage = Storage.parse_obj(self.last_block['storage'])

        for key, data_list in self.last_block['storage']['storage'].items():
            for data in data_list:
                if key not in storage.storage or key == 'root':
                    storage.storage[key] = [Data.parse_obj(data)]
                    continue
                storage.storage[key].append(Data.parse_obj(data))

        return storage

    def __merge_storage(self) -> Storage:
        """
        Слияние неподтвержденного хранилища и хранилища в блоке.

        :return: Актуальное хранилище
        :rtype: Storage
        """
        storage = self.last_block['storage']

        if not isinstance(storage, Storage):
            storage = self.__storage_representation()

        storage = storage.copy(deep=True)

        for key, value in self.__unconfirmed_insert:
            try:
                storage.insert(key=key, data=value)
            except Exception:
                continue

        for key, id, new_data in self.__unconfirmed_update:
            try:
                storage.update(key=key, id=UUID(id), new_data=new_data)
            except Exception:
                continue

        for key, id, data in self.__unconfirmed_delete:
            try:
                if id:
                    storage.delete(key=key, id=UUID(id), data=data)
                else:
                    storage.delete(key=key, data=data)
            except Exception:
                continue

            if storage.get(key=key) == []:
                storage.delete(key=key)

        self.__unconfirmed_insert.clear()
        self.__unconfirmed_update.clear()
        self.__unconfirmed_delete.clear()

        return storage

    @property
    def chain(self) -> list[Block]:
        """
        Получение неглубокой копии цепи.

        :return: Копия блокчейна 
        :rtype: list[Block]
        """
        return self.__chain.copy()

    @property
    def last_block(self) -> Block | dict:
        """
        Получение копии крайнего, актуального блока в цепи.

        :return: Копия крайнего, актуального блока в цепи
        :rtype: Block or dict
        """
        return self.__chain[-1].copy()

    @staticmethod
    def serializable(object: Any) -> dict[str, Any]:
        """
        Разрешение сериализации цепи блокчейна.
        Используется в параметре default метода json.dumps() / json.dump().

        :return: Словарь __dict__ объекта object, если isinstance(object, UUID) - возвращает идентификатор в формате строки
        :rtype: dict[str, Any]
        """
        if isinstance(object, UUID):
            return str(object)
        return object.__dict__

    @staticmethod
    def hash(block: Block) -> str:
        """
        Вычисление хэша блока по алгоритму Стрибог 256 ГОСТ 34.11-2018.

        :param Block block: Блок
        :return: Хэш блока в виде шестнадцатеричной строки, полученный по алгоритму Стрибог 256
        :rtype: str
        """
        if isinstance(block, dict):
            return gosthash.new('streebog256', data=json.dumps(block, sort_keys=True).encode()).hexdigest()
        return gosthash.new('streebog256', data=block.json(sort_keys=True).encode()).hexdigest()

    @staticmethod
    def validate_proof(difficulty: float, proof: int, previous_proof: int, previous_hash: str) -> bool:
        """
        Проверка работы, действительно ли хэш для доказательства proof содержит 1 * difficulty конечных нулей.
        Вычисление хэша блока по алгоритму Стрибог 256 ГОСТ 34.11-2018.

        :param float difficulty: Сложность алгоритма PoW блока на момент решения доказательства
        :param int proof: Доказательсво работы
        :param int previous_proof: Доказательство работы предыдущего блока
        :param str previous_hash: Хэш предыдущего блока
        :return: Результат проверки доказательства
        :rtype: bool
        """
        guess = f'{previous_proof}{proof}{previous_hash}'.encode()
        return gosthash.new('streebog256', data=guess).hexdigest().endswith('0' * int(difficulty))

    def validate_chain(self, chain: list[Block]) -> bool:
        """
        Определения, является ли блокчейн валидным. 
        Проверка производится путем вычисления и сверки хэшей блоков, проверки доказательства PoW.

        :param list[Block] chain: Проверяемая версия блокчейна
        :return: Результат проверки блокчейна на валидность
        :rtype: bool
        """
        # Попараная итерация
        # >>> a = [0, 1, 2, 3, 4, 5]
        # >>> print(a[:-1])
        # [0, 1, 2, 3, 4]
        # >>> print(a[1:])
        # [1, 2, 3, 4, 5]
        # >>> print(list(zip(a[:-1], a[1:])))
        # [(0, 1), (1, 2), (2, 3), (3, 4), (4, 5)]

        for previous, current in zip(chain[:-1], chain[1:]):
            if current['previous_hash'] != self.hash(previous):
                return False

            if not self.validate_proof(difficulty=current['difficulty'], proof=current['proof'],
                                       previous_proof=previous['proof'], previous_hash=self.hash(previous)):
                return False
        return True

    def __sync_nodes(self) -> bool:
        """
        Алгоритм синхронизации действующих узлов сети.
        Принцип работы заключается в опросе существующего списка узлов, запросе списка узлов узла.  
        При получении в ответ списка узлов опрашиваемого узла, объединяется текущее множество узлов с полученным
        и продолжается дальнейший опрос.
        Если код ответа опрашиваемого узла не равен 200 - он удаляется из текущего множества узлов.

        :return: Была ли выполнена синхронизация, т.е. отличается ли состояние до синхранизации от состояния после
        :rtype: bool
        """
        old_nodes = self.nodes.copy()

        for node in old_nodes:
            try:
                response = requests.get(
                    f'http://{node}/nodes', timeout=3, verify=False)
            except Exception:
                self.nodes.discard(node)
            else:
                if response.status_code != 200:
                    self.nodes.discard(node)
                    continue
                self.nodes.update(response.json()['nodes'])

        updated_nodes = self.nodes.copy()

        for node in updated_nodes:
            try:
                requests.post(
                    f'http://{node}/nodes', data={'nodes': self.nodes}, timeout=3, verify=False)
            except Exception:
                self.nodes.discard(node)

        if self.nodes != old_nodes:
            return True

        return False

    def __do_consensus(self) -> None:
        """
        Алгоритм синхронизации цепи блокчейна между действующими узлами сети.
        Принцип работы заключается в опросе существующего списка узлов, запросе выполнения алгоритма консенсуса узлом.  
        Если код ответа опрашиваемого узла не равен 200 - он удаляется из текущего множества узлов.

        :rtype: None
        """
        nodes = self.nodes.copy()

        for node in nodes:
            try:
                response = requests.get(
                    f'http://{node}/consensus', timeout=3, verify=False)
            except Exception:
                self.nodes.discard(node)
            else:
                if response.status_code != 200:
                    self.nodes.discard(node)
                    continue

    def consensus(self) -> bool:
        """
        Алгоритм консенсуса, разрешает конфликты, заменяя локальную копию блокчейна на самую длинную (актуальную) в сети.
        Принцип работы заключается в опросе существующего списка узлов, 
        запросе копии блокчейна с последующей проверкой цепи.
        Также разрешается список актуальных узлов сети.
        Если код ответа опрашиваемого узла не равен 200 - он удаляется из текущего множества узлов.

        :return: Была ли заменена локальная копия
        :rtype: bool
        """
        self.__sync_nodes()
        new_chain = None
        max_length = len(self.chain)
        nodes = self.nodes.copy()

        for node in nodes:
            try:
                response = requests.get(f'http://{node}/chain', timeout=3)
            except Exception:
                self.nodes.discard(node)
            else:
                if response.status_code != 200:
                    self.nodes.discard(node)
                    continue

                if response.status_code == 200:
                    length = response.json()['length']
                    chain = response.json()['chain']

                    if length > max_length and self.validate_chain(chain=chain):
                        max_length = length
                        new_chain = chain

        if new_chain:
            self.__chain = new_chain
            return True

        return False

    def proof_of_work(self) -> Tuple[int, float]:
        """
        Алгоритм доказательства работы:

         - Найти число p' такое, чтобы hash(pp'h) содержал 1 * Blockchain.difficulty конечных нулей
         - Где p - предыдущее доказательство, h - хэш предыдщего блока, а p' - новое доказательство.

        После успешного найденного доказательства - сложность увеличивается на const

        :return: Доказательство работы, сложность алгоритма PoW
        :rtype: Tuple[int, float]
        """
        difficulty = self.last_block['difficulty'] + 0.01

        nonce = 0
        while self.validate_proof(difficulty, nonce, self.last_block['proof'], self.hash(self.last_block)) is False:
            nonce += 1

        return nonce, difficulty

    def register_node(self, address: str) -> None:
        """
        Добавление нового узла в список узлов.

        :param str address: IPv4 Адрес узла, например: 'http://192.168.0.5:5000' или '192.168.0.5:5000' или '192.168.0.5'
        :rtype: None
        :raises ValueError: Неверный формат URL адреса или IP адрес невалидный
        """

        splitted = address.split('//')[-1].split(':')
        port = str()
        host = splitted[0]

        addr = IPv4Address(address=host)

        if len(splitted) == 2:
            port = f':{splitted[1]}'

        # TODO: Remove on production usage. For dev
        # if not addr.is_private:
        #    raise ValueError(INVALID_NODE_URL)

        # TODO: Uncomment on production usage
        # if not addr.is_global:
        #   raise ValueError(INVALID_NODE_URL)
        address = f'{addr}{port}'
        self.nodes.add(address)

    def create_block(self, proof: int, difficulty: float) -> Block | None:
        """
        Создание нового блока в блокчейне.

        :param int proof: Доказательство работы
        :param float difficulty: Сложность алгоритма PoW
        :return: Копия добавленного блока или None, если проверка доказательства работы не пройдена
        :rtype: Block or None
        """
        if not self.validate_proof(difficulty=difficulty, proof=proof,
                                   previous_proof=self.last_block['proof'], previous_hash=self.hash(self.last_block)):
            return None

        block = Block(
            index=self.last_block['index'] + 1,
            proof=proof,
            difficulty=difficulty,
            previous_hash=self.hash(self.last_block),
            previous_proof=self.last_block['proof'],
            storage=self.__merge_storage())

        self.__chain.append(block)
        self.__do_consensus()

        return self.last_block

    def new_data(self, key: str, data: Data) -> int:
        """
        Добавление данных в хранилище.
        В случае неудачной транзакции, предоставленные данные не будут добавлены.

        :param str key: Публичный ключ пользователя
        :param Data data: Ресурс
        :return: Индекс блока, который будет содержать новое состояние данных
        :rtype: int
        """
        self.__unconfirmed_insert.append((key, data))
        return self.last_block['index'] + 1

    def get_data(self, key: str, obtained: Optional[UUID] = None) -> list[Data] | Data | None:
        """
        Получение данных из крайнего блока актуальной цепи блокчейна.

        :param str key: Публичный ключ пользователя
        :param Optional[UUID] obtained: Идентификатор ресурса в хранилище, если None - возвращается хранилище ключа
        :return: Список данных или объект Data или None, если данные не найдены
        :rtype: list[Data] | Data | None
        """
        if not isinstance(self.last_block['storage'], Storage):
            return self.__storage_representation().get(key=key, id=obtained)
        return self.last_block['storage'].get(key=key, id=obtained)

    def update_data(self, key: str, renewing: UUID, new_data: UpdateData) -> int:
        """
        Обновление данных в хранилище.
        В случае неудачной транзакции, предоставленные изменения не будут применены.

        :param str key: Публичный ключ пользователя
        :param UUID renewing: Идентификатор ресурса в хранилище
        :param UpdateData new_data: Обновленные данные ресурса
        :return: Индекс блока, который будет содержать новое состояние данных
        :rtype: int
        """
        self.__unconfirmed_update.append((key, renewing, new_data))
        return self.last_block['index'] + 1

    def delete_data(self, key: str, removal: Optional[UUID] = None, data: Optional[Data] = None) -> int:
        """
        Удаление данных из хранилища, либо удаление хранилища ключа key, если removal и data = None.
        В случае неудачной транзакции, предоставленные изменения не будут применены.

        :param str key: Публичный ключ пользователя
        :param Optional[UUID] removal: Идентификатор ресурса в хранилище
        :param Optional[Data] data: Удаляемый ресурс в виде обьекта Data
        :return: Индекс блока, который будет содержать новое состояние данных
        :rtype: int
        """
        self.__unconfirmed_delete.append((key, removal, data))
        return self.last_block['index'] + 1

    def json(self) -> str:
        """
        Сериализация цепи блокчейна в строку в формате JSON.

        :return: Сериализованная строка в формате JSON
        :rtype: str
        """
        return json.dumps(obj=self.chain, default=self.serializable)
