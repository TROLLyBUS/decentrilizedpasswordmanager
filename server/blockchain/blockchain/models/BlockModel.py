from datetime import datetime
from pydantic import BaseModel, Field

from ...storage.models.StorageModel import Storage


class Block(BaseModel):
    """
    Блок цепи
    """
    index: int
    proof: int
    difficulty: float
    previous_hash: str
    previous_proof: int
    storage: Storage
    timestamp: str = Field(
        default_factory=lambda: datetime.utcnow().isoformat())
    
    def __getitem__(self, item):
        return getattr(self, item)