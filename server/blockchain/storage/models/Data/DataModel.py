from uuid import UUID, uuid4
from pydantic import BaseModel, Field
from datetime import datetime


class Data(BaseModel):
    """
    Пользовательский ресурс
    """
    id: UUID = Field(default_factory=uuid4)
    resource: str
    password: str
    description: str = None
    created_at: str = Field(
        default_factory=lambda: datetime.utcnow().isoformat())
    updated_at: str = None
