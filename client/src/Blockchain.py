import requests

from typing import Optional
from src.Cipher import Cipher


class Blockchain:
    """
    Реализация API интерфейса блокчейна.
    """

    def __init__(self, nodes: list[str]):
        self.nodes: list[str] = nodes
        self.__encoder = None

    def set_encoder(self, encoder: Cipher):
        """
        Установка шифровальщика, который обрабатывает данные в запросах к Blockchain API.

        :param encoder: Обьект шифровальщика
        :type encoder: Cipher
        """
        self.__encoder = encoder

    def mine(self) -> dict | None:
        """
        Добыча нового блока в сети блокчейна.
        Фиксация изменений данных.

        :return: Добытый блок или None
        :rtype: dict | None
        """
        try:
            response = requests.get(
                f'http://{self.nodes[0]}/mine', verify=False)
        except Exception:
            self.nodes = self.nodes[1:]
            return None
        else:
            if response.status_code == 500:
                self.nodes = self.nodes[1:]
                return None

            if response.status_code == 400:
                return None

        return response.json()

    def refresh_nodes(self) -> bool:
        """
        Получение актуального списка узлов сети, обновление локального.

        :return: Был ли обновлён список узлов сети
        :rtype: bool
        """
        try:
            response = requests.get(
                f'http://{self.nodes[0]}/nodes', timeout=3, verify=False)
        except Exception:
            self.nodes = self.nodes[1:]
            return False
        else:
            if response.status_code != 200:
                self.nodes = self.nodes[1:]
                return False

        self.nodes = response.json()['nodes']
        return True

    def create_resource(self, resource: str, password: str, description: Optional[str] = None) -> dict | None:
        """
        Создание ресурса пользователя.
        В случае неудачной транзакции, предоставленные данные не будут добавлены.

        :param resource: Название ресурса
        :type resource: str
        :param password: Пароль ресурса
        :type password: str
        :param description: Описание ресурса, по умолчанию равняется None
        :type description: str, None
        :return: Индекс блока, который будет содержать новое состояние данных. Если запрос не выполнен - None
        :rtype: dict | None
        """
        resource, password, description = (self.__encoder.encrypt(value=resource), self.__encoder.encrypt(
            value=password), self.__encoder.encrypt(value=description) if description else None)

        try:
            response = requests.post(
                f'http://{self.nodes[0]}/storage/{self.__encoder.id}?resource={resource}&password={password}{f"&description={description}" if description else ""}', data='', timeout=3, verify=False)
        except Exception:
            self.nodes = self.nodes[1:]
            return None
        else:
            if response.status_code == 400:
                return None
            if response.status_code == 500:
                self.nodes = self.nodes[1:]
                return None
        return response.json()

    def get_storage(self) -> dict | None:
        """
        Получение хранилища пользователя.

        :return: Хранилище пользователя если существует, иначе None
        :rtype: dict or None
        """
        try:
            response = requests.get(
                f'http://{self.nodes[0]}/storage/{self.__encoder.id}', timeout=3, verify=False)
        except Exception:
            self.nodes = self.nodes[1:]
            return None
        else:
            if response.status_code == 404:
                return None
            if response.status_code == 500:
                self.nodes = self.nodes[1:]
                return None
        storage = response.json()['storage']

        for item in storage:
            item['resource'], item['password'], item['description'] = (self.__encoder.decrypt(
                value=item['resource']), self.__encoder.decrypt(value=item['password']), self.__encoder.decrypt(value=item['description']) if item['description'] else item['description'])

        return storage

    def delete_storage(self) -> dict | None:
        """
        Удаление хранилища пользователя.
        В случае неудачной транзакции, предоставленные изменения не будут применены.

        :return: Индекс блока, который будет содержать новое состояние данных. Если запрос не выполнен - None
        :rtype: dict | None
        """
        try:
            response = requests.delete(
                f'http://{self.nodes[0]}/storage/{self.__encoder.id}', timeout=3, verify=False)
        except Exception:
            self.nodes = self.nodes[1:]
            return None
        else:
            if response.status_code == 400:
                return None
            if response.status_code == 500:
                self.nodes = self.nodes[1:]
                return None
        return response.json()

    def get_resource(self, id: str) -> dict | None:
        """
        Получение данных из крайнего блока актуальной цепи блокчейна.

        :param id: Идентификатор ресурса в хранилище.
        :type id: str
        :return: Ресурс пользователя если существует, иначе None
        :rtype: dict or None
        """
        try:
            response = requests.get(
                f'http://{self.nodes[0]}/storage/{self.__encoder.id}/{id}', timeout=3, verify=False)
        except Exception:
            self.nodes = self.nodes[1:]
            return None
        else:
            if response.status_code in [400, 404]:
                return None
            if response.status_code == 500:
                self.nodes = self.nodes[1:]
                return None
        return response.json()

    def patch_resource(self, id: str, resource: Optional[str] = None, password: Optional[str] = None, description: Optional[str] = None) -> dict | None:
        """
        Изменение ресурса.
        В случае неудачной транзакции, предоставленные изменения не будут применены.

        :param id: Идентификатор ресурса в хранилище
        :type id: str
        :param resource: Название ресурса, по умолчанию равняется None
        :type resource: Optional[str]
        :param password: Пароль ресурса, по умолчанию равняется None
        :type password: Optional[str]
        :param description: Описание ресурса, по умолчанию равняется None
        :type description: Optional[str]
        :return: Индекс блока, который будет содержать новое состояние данных. Если запрос не выполнен - None
        :rtype: dict | None
        """
        if all(v is None for v in [resource, password, description]):
            return None

        resource, password, description = (
            self.__encoder.encrypt(value=resource) if resource else None,
            self.__encoder.encrypt(value=password) if password else None,
            self.__encoder.encrypt(value=description) if description else None
        )

        try:
            response = requests.patch(
                f'http://{self.nodes[0]}/storage/{self.__encoder.id}/{id}?resource={resource}&password={password}{f"&description={description}" if description else ""}', data='', timeout=3, verify=False)
        except Exception:
            self.nodes = self.nodes[1:]
            return None
        else:
            if response.status_code == 400:
                return None
            if response.status_code == 500:
                self.nodes = self.nodes[1:]
                return None
        return response.json()

    def delete_resource(self, id: str) -> dict | None:
        """
        Удаление данных из хранилища.
        В случае неудачной транзакции, предоставленные изменения не будут применены.

        :param id: Идентификатор ресурса в хранилище
        :type id: str
        :return: Индекс блока, который будет содержать новое состояние данных. Если запрос не выполнен - None
        :rtype: dict | None
        """
        try:
            response = requests.delete(
                f'http://{self.nodes[0]}/storage/{self.__encoder.id}/{id}', timeout=3, verify=False)
        except Exception:
            self.nodes = self.nodes[1:]
            return None
        else:
            if response.status_code == 400:
                return None
            if response.status_code == 500:
                self.nodes = self.nodes[1:]
                return None
        return response.json()
