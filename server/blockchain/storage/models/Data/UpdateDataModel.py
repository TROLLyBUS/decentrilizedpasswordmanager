from typing import Optional
from pydantic import BaseModel


class UpdateData(BaseModel):
    """
    Обновление пользовательского ресурса
    """
    resource: Optional[str]
    password: Optional[str]
    description: Optional[str]
