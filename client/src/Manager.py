import json
import os
from typing import Any


class Manager:
    """
    Реализация логики работы с файлом конфигурации программы и иной, менее приоритетной функциональностью.
    """

    def __init__(self) -> None:
        self.nodes: list[str] = self.__load_config()['nodes']
        self.__seed: str = self.__load_config()['seed']

    def __load_config(self) -> dict:
        """
        Чтение или создание конфигурационного файла, если не существует.

        :return: Конфигурация программы
        :rtype: dict
        """
        if not os.path.exists('config.json'):
            with open('config.json', 'w') as f:
                json.dump({'nodes': [], 'seed': []}, f)

        with open('config.json', 'r') as f:
            return json.load(f)

    def __save_config(self, config: dict) -> None:
        """
        Сохранение конфигурационного файла программы.

        :param config: Конфигурация программы
        :type config: dict
        :rtype: None
        """
        with open('config.json', 'w') as f:
            json.dump(config, f)

    def config_set(self, key: str, value: str) -> None:
        """
        Установка переменной в конфигурационном файле программы.

        :param key: Ключ
        :type key: str
        :param value: Значение
        :type value: str
        :rtype: None
        """
        config = self.__load_config()
        config[key] = value
        self.__save_config(config=config)

    def config_get(self, key: str) -> Any:
        """
        Чтение переменной в конфигурационном файле программы.

        :param key: Ключ
        :type key: str
        :return: Значение ключа key в файле конфигурации
        :rtype: Any
        """
        config = self.__load_config()

        if key in config:
            return config[key]
        return None

    @property
    def seed(self) -> str:
        """
        Получение сид-фразы пользователя.

        :return: Сид-фраза пользователя
        :rtype: str
        """
        return self.__seed

    @seed.setter
    def seed(self, values: list[str]) -> None:
        """
        Установка сид-фразы.

        :param values: Список слов, составляющих ключевую фразу пользователя
        :type values: list[str]
        :rtype: None
        """
        self.__seed = values
        self.config_set(key='seed', value=self.__seed)
