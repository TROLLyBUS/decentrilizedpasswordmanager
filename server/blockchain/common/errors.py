UUID_EXISTS = 'The element with the given UUID already exists'
UUID_NOT_FOUND='The element with the given UUID was not found'
STORAGE_EMPTY='The storage for this key is empty'
INVALID_NODE_URL='Invalid node URL provided'
INVALID_STORAGE_RECOVERY_TYPE='Invalid storage type for recovery, must be dict'