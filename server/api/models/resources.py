from typing import List
from flask_restx.reqparse import RequestParser

post_resource = RequestParser(bundle_errors=True)
post_resource.add_argument(
    name='resource', type=str, required=True, nullable=False)
post_resource.add_argument(
    name='password', type=str, required=True, nullable=False)
post_resource.add_argument(
    name='description', type=str, required=False, nullable=True)

patch_resource = RequestParser(bundle_errors=True)
patch_resource.add_argument(
    name='resource', type=str, required=False, nullable=True, default=None)
patch_resource.add_argument(
    name='password', type=str, required=False, nullable=True, default=None)
patch_resource.add_argument(
    name='description', type=str, required=False, nullable=True, default=None)

post_nodes = RequestParser(bundle_errors=False)
post_nodes.add_argument(
    name='nodes', action='append', type=str, required=True, nullable=True)
