import PySimpleGUI as sg

sg.theme('Reddit')

enter_layout = [
    [sg.Text('Пожалуйста, выберите..')], 
    [sg.Button('Логин', key='-LOGIN-', size=(20, 1), border_width=0)],
    [sg.Button('Регистрация', key='-REGISTER-', size=(20, 1), border_width=0)]
]

login_layout = [
    [sg.Text('Введите ключевую фразу..')],
    *[[sg.Text(f'{i + 1}.', size=(2, 0)), sg.Input(key=f'-KEY-IN-{i}-', size=(20, 1), pad=(6, 10), border_width=0), 
       sg.Text(f'{i + 2}.', size=(2, 0)), sg.Input(key=f'-KEY-IN-{i + 1}-', size=(20, 1), border_width=0)] for i in range(0, 8, 2)],
    [sg.Push(), sg.Button('Подтвердить', key='-LOGIN-CONFIRM-', size=(20, 1), border_width=0), sg.Push(), sg.Button('Назад', key='-BACK-', size=(20, 1), border_width=0), sg.Push()]
]

register_layout = [
    [sg.Text('Пожалуйста, запишите ключевую фразу.\nОна будет использована для доступа к учетной записи.')],
    *[[sg.Text(f'{i + 1}.', size=(2, 0)), sg.Input(key=f'-KEY-OUT-{i}-', size=(20, 1), readonly=True, pad=(6, 10), border_width=0), 
       sg.Text(f'{i + 2}.', size=(2, 0)), sg.Input(key=f'-KEY-OUT-{i + 1}-', readonly=True, size=(20, 1), border_width=0)] for i in range(0, 8, 2)],
    [sg.Push(), sg.Button('Подтвердить', key='-REGISTER-CONFIRM-', size=(20, 1), border_width=0), sg.Push(), sg.Button('Назад', key='-BACK-', size=(20, 1), border_width=0), sg.Push()]
]

main_layout = [
    [sg.Column([[sg.Menu([['Меню', ['Обновить', 'Выход из аккаунта', 'Удалить хранилище']]], visible=False, key='-MENU-')], 
                [sg.Frame('Ресурсы', [[sg.Column([[sg.Listbox(values=[], key='-LIST-ACC-', bind_return_key = True, right_click_menu=['&Right', ['Удалить ресурс', 'Просмотреть / Изменить ресурс']], size=(28, 40))]], size=(200, 340))]], size=(220, 385))]], pad=(0, 0)),
     sg.Column([
        [sg.Frame('Информация', [
            [sg.Text(), sg.Column([[sg.Text('Ресурс')],
            [sg.Input(key='-IN-RES-', size=(27, 0), border_width=0)],
            [sg.Text('Пароль')],
            [sg.Input(key='-IN-PASS-', size=(27, 0), border_width=0)],
            [sg.Text('Описание')],
            [sg.Multiline(key='-IN-DESC-', size=(27, 10), border_width=0, autoscroll=True, no_scrollbar=True)],
            [sg.VPush()],
            [sg.Button('Сохранить / Создать', key='-SAVE-', border_width=0), sg.Button('Очистить', key='-CLEAR-', border_width=0)]], size=(255, 365), pad=(10, 0))]
        ])], 
    ], pad=(0, 0))]
]