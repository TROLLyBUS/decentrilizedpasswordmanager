from gostcrypto import gostcipher, gosthash


class Cipher:
    """
    Реализация шифрования и дешифрования пользовательских данных
    """

    def __init__(self, seed: list[str]) -> None:
        self.id = gosthash.new('streebog256', data=(
            '.'.join(seed)).encode()).hexdigest()
        self.__cipher_obj = gostcipher.new('kuznechik',
                                           gosthash.new('streebog256', data='.'.join(
                                               seed).encode()).digest(),
                                           gostcipher.MODE_ECB,
                                           pad_mode=gostcipher.PAD_MODE_1)

    def encrypt(self, value: str) -> str:
        """
        Шифрование пользовательских данных с помощью алгоритма Кузнечик ГОСТ 34.12-2018.

        :param value: Данные для шифрования
        :type value: str
        :return: Зашифрованные данные (hex)
        :rtype: str
        """
        return self.__cipher_obj.encrypt(value.encode('utf-16')).hex()

    def decrypt(self, value: str) -> str:
        """
        Расшифровывание пользовательских данных с помощью алгоритма "Кузнечик" ГОСТ 34.12-2018.

        :param value: Зашифрованные пользовательские данные (hex)
        :type value: str
        :return: Расшифровыванные данные
        :rtype: str
        """
        return self.__cipher_obj.decrypt(bytearray.fromhex(value)).decode('utf-16').rstrip('\x00')
