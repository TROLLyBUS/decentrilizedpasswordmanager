import json
import sys

from typing import Optional, Tuple
from api.api import app, blockchain


def valid(host: str) -> bool:
    """
    Валидация сетевого адреса узла хоста.

    :return: Валидный ли переданный хост адрес
    :rtype: bool
    """
    return host not in ['0.0.0.0', 'localhost', '127.0.0.0', '127.0.0.1']


def init_args() -> Tuple[str, Optional[int], Optional[str]]:
    """
    Инициализация аргументов при запуске узла.

    :raises ValueError: Невалидный хост адрес
    :return: Кортеж аргументов, хостовой адрес, порт, список предварительно загруженных из файла узлов
    :rtype: Tuple[str, Optional[int], Optional[str]]
    """
    from argparse import ArgumentParser

    parser = ArgumentParser()
    parser.add_argument('--host', required=True, type=str,
                        help='the public IPv4 address of the node available on the Internet. Required to reach a node by other nodes')
    parser.add_argument('-p', '--port', required=True, default=5100,
                        type=int, help='the public port of the node, accessible on the Internet. Required to reach a node by other nodes')
    parser.add_argument('-lp', '--local-port', required=False, default=5100,
                        type=int, help='the host port of the node where the blockchain API will be deployed')
    parser.add_argument('-f', '--file', default=None, required=True,
                        type=str, help='the path to the JSON file containing the list of node addresses (see nodes.example.json)')
    args = parser.parse_args()

    if not valid(host=args.host):
        raise ValueError(
            "Invalid node address, invalid values included in ['0.0.0.0', 'localhost', '127.0.0.1', '127.0.0.0']")

    nodes = None
    if args.file:
        with open(args.file) as f:
            nodes = json.load(f).get('nodes')
    return args.host, args.port, args.local_port, nodes


if __name__ == '__main__':
    host, port, local_port, nodes = init_args()
    if not nodes:
        sys.exit(
            'It is necessary to provide at least one active node of the network to use the consensus algorithm')

    for node in nodes:
        blockchain.register_node(node)
    blockchain.register_node(f'{host}:{port}')

    app.run(host='0.0.0.0', port=local_port, debug=False)
