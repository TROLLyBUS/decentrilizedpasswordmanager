.. Decentralized Password Storage documentation master file, created by
   sphinx-quickstart on Fri Mar 31 13:20:53 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Decentralized Password Storage's documentation!
==========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
