NODES_INVALID = 'Invalid node list, please provide a valid node list'
RESOURCE_NOT_FOUND = 'Resource not found'
BLOCK_NOT_MINED = 'Block mining ended without success'
BLOCK_MINED = 'The block has been successfully mined'
ROOT_CANNOT_MODIFIED = 'The root storage object cannot be modified'

CHAIN_REPLACED = 'The chain has been replaced'
CHAIN_MAJOR = 'The chain is authoritative'
RESOURCE_STATE_CHANGED = 'The changes will be applied to the storage in the block with the specified index as soon as it is obtained. In case of a failed transaction, the changes provided will not be applied'