import PySimpleGUI as sg
import random


from english_words import get_english_words_set
from src.utils import isEnglish
from src.Manager import Manager
from src.Cipher import Cipher
from src.Blockchain import Blockchain
from src.ui.layouts import main_layout, login_layout, register_layout, enter_layout

layouts = [
    [
        sg.Column(enter_layout, key='-COL-ENTER-', element_justification='c'),
        sg.Column(main_layout, key='-COL-MAIN-',
                  visible=False, element_justification='c'),
        sg.Column(login_layout, key='-COL-LOGIN-',
                  visible=False, element_justification='l'),
        sg.Column(register_layout, key='-COL-REGISTER-',
                  visible=False, element_justification='c')
    ]
]
window = sg.Window('Децентрализованный менеджер паролей',
                   layouts, finalize=True, font=('JetBrains Mono NL', 10), grab_anywhere=True)

manager = Manager()
blockchain = Blockchain(nodes=manager.config_get('nodes'))
previous, current = (None, '-COL-ENTER-')


def change_layouts(current: str, to: str) -> None:
    """
    Смена layout`ов.

    :param current: Заменяемый layout
    :type current: str
    :param to: Заменяющий layout
    :type to: str

    :rtype: None
    """
    window['-MENU-'].update(visible=False)

    if to == '-COL-MAIN-':
        window['-MENU-'].update(visible=True)

    window[current].update(visible=False)
    window[to].update(visible=True)


def refresh():
    """
    Запрос актуальных данных.
    Обнолвение Listbox, при загрузке главного layout`а.
    """
    storage = blockchain.get_storage()
    window['-LIST-ACC-'].update([f'{i+1}. {item["resource"]}' for i,
                                 item in enumerate(storage)] if storage else [])
    window['-LIST-ACC-'].metadata = storage if storage else []


def clear_fields():
    """
    Очистка полей элементов главного layout`а
    """
    window['-IN-RES-'].update('')
    window['-IN-PASS-'].update('')
    window['-IN-DESC-'].update('')
    window['-LIST-ACC-'].SetValue([])


# window.FindElementWithFocus()
if __name__ == '__main__':
    window['-MENU-'].update(visible=False)
    window.refresh()

    if current == '-COL-ENTER-' and manager.config_get('seed'):
        change_layouts(current=current, to='-COL-MAIN-')
        previous, current = (current, '-COL-MAIN-')
        blockchain.set_encoder(Cipher(manager.config_get('seed')))

    if not manager.config_get('nodes'):
        sg.Popup(
            'Предоставлен конфигурационный файл (config.json), который не содержит ни одного узла сети ({"nodes": []}).\nВыполнение подключения невозможно.\n\nДля дальнейшей работы, предоставьте корректный конфигурационный файл, содержащий IPv4 адрес минимум одного актуального узла сети.', font=('JetBrains Mono NL', 10), title='Внимание!', keep_on_top=True)
        window.close()
    blockchain.refresh_nodes()

    if not blockchain.nodes and manager.config_get('nodes'):
        sg.Popup('Cеть блокчейна не содержит функционирующих узлов.\nВыполнение подключения невозможно.\n\nДля дальнейшей работы, предоставьте корректный конфигурационный файл (config.json), содержащий IPv4 адрес минимум одного актуального узла сети.', font=(
            'JetBrains Mono NL', 10),  title='Внимание!', keep_on_top=True)
        window.close()
    refresh()

    while True:
        event, values = window.read()
        print(event, values)

        if event == sg.WIN_CLOSED:
            break

        if not blockchain.nodes:
            sg.Popup('Cеть блокчейна не содержит функционирующих узлов.\nВыполнение подключения невозможно.\n\nДля дальнейшей работы, предоставьте корректный конфигурационный файл (config.json), содержащий IPv4 адрес минимум одного актуального узла сети.', font=(
                'JetBrains Mono NL', 10),  title='Внимание!', keep_on_top=True)
            break

        if event.startswith('-BACK-') and previous != None:
            change_layouts(current=current, to=previous)
            previous, current = (None, previous)
            manager.seed = []

        if event == '-LOGIN-':
            for x in range(8):
                window[f'-KEY-IN-{x}-'].update('')
            change_layouts(current=current, to='-COL-LOGIN-')
            previous, current = (current, '-COL-LOGIN-')

        if event == '-REGISTER-':
            change_layouts(current=current, to='-COL-REGISTER-')
            previous, current = (current, '-COL-REGISTER-')

            seed = [x.lower() for x in random.sample(
                get_english_words_set(['web2'], lower=True, alpha=True), 8)]

            for i in range(8):
                window[f'-KEY-OUT-{i}-'].update(seed[i])
            manager.seed = seed
            blockchain.set_encoder(Cipher(seed))

        if event == '-LOGIN-CONFIRM-':
            seed = [values[f'-KEY-IN-{x}-'].lower() for x in range(8)]
            valid = True

            for item in seed:
                if not item or not item.isalpha() or len(item) < 3 or not isEnglish(item):
                    sg.Popup('Некорретный ввод или необходимо заполнить все поля!\nДопустимы только буквенные значения латинского алфавита, длиной не менее 4 символов.',
                             title='Внимание!', font=('JetBrains Mono NL', 10), keep_on_top=True)
                    valid = False
                    break

            if valid:
                manager.seed = seed
                blockchain.set_encoder(Cipher(seed))

                if not blockchain.get_storage():
                    sg.Popup('Введённой seed-фразы не существует!\nПройдите этап регистрации.',
                             title='Внимание!', font=('JetBrains Mono NL', 10), keep_on_top=True)
                    for x in range(8):
                        window[f'-KEY-IN-{x}-'].update('')
                    window['-BACK-'].click()
                    continue

                window['-CLEAR-'].click()
                change_layouts(current=current, to='-COL-MAIN-')
                previous, current = (current, '-COL-MAIN-')

        if event == '-REGISTER-CONFIRM-':
            window['-CLEAR-'].click()
            change_layouts(current=current, to='-COL-MAIN-')
            previous, current = (current, '-COL-MAIN-')
            refresh()

        if event == '-SAVE-':
            if not values['-IN-RES-'] and not values['-IN-PASS-']:
                sg.Popup('Заполните все поля!',
                         title='Внимание!', font=('JetBrains Mono NL', 10), keep_on_top=True)
                window['-CLEAR-'].click()
                continue

            if values['-LIST-ACC-']:
                storage = window['-LIST-ACC-'].metadata
                index = int(values['-LIST-ACC-'][0].split('.')[0]) - 1
                data = storage[index]

                if data['resource'] == values['-IN-RES-'] and data['password'] == values['-IN-PASS-'] and data['description'] == values['-IN-DESC-']:
                    continue

                blockchain.patch_resource(id=data['id'], resource=values['-IN-RES-'],
                                          password=values['-IN-PASS-'], description=values['-IN-DESC-'])
                blockchain.mine()

                window['-CLEAR-'].click()
                refresh()

            if not values['-LIST-ACC-']:
                blockchain.create_resource(
                    resource=values['-IN-RES-'], password=values['-IN-PASS-'], description=values['-IN-DESC-'])
                blockchain.mine()

                window['-CLEAR-'].click()
                refresh()

        if event == '-CLEAR-':
            clear_fields()

        if event == 'Удалить ресурс':
            if not values['-LIST-ACC-']:
                sg.Popup('Выберите обьект для удаления из списка!',
                         title='Внимание!', font=('JetBrains Mono NL', 10), keep_on_top=True)
                continue

            index = int(values['-LIST-ACC-'][0].split('.')[0]) - 1
            blockchain.delete_resource(
                id=window['-LIST-ACC-'].metadata[index]['id'])
            blockchain.mine()

            window['-CLEAR-'].click()
            refresh()

        if event == 'Просмотреть / Изменить ресурс' and values['-LIST-ACC-']:
            storage = window['-LIST-ACC-'].metadata
            index = int(values['-LIST-ACC-'][0].split('.')[0]) - 1
            window['-IN-RES-'].update(storage[index]['resource'])
            window['-IN-PASS-'].update(storage[index]['password'])
            window['-IN-DESC-'].update(storage[index]['description']
                                       if storage[index]['description'] else '')
            window['-IN-RES-'].set_focus(True)

        if event == 'Обновить':
            window['-CLEAR-'].click()
            refresh()

        if event == 'Удалить хранилище':
            if blockchain.get_storage():
                if blockchain.delete_storage():
                    blockchain.mine()

                    window['-CLEAR-'].click()
                    refresh()

        if event == 'Выход из аккаунта':
            change_layouts(current=current, to='-COL-ENTER-')
            previous, current = (current, '-COL-ENTER-')
            manager.seed = []

    manager.config_set(key='nodes', value=blockchain.nodes)
    window.close()
